package Models

import "github.com/go-playground/validator/v10"

type User struct {
	Email     string `form:"email" json:"email" xml:"email" validate:"required,email"`
	FirstName string `form:"first_name" json:"first_name" xml:"first_name" validate:"required"`
	LastName  string `form:"last_name" json:"last_name" xml:"last_name" validate:"required"`
	Password  string `form:"password" json:"password" xml:"password" validate:"required"`
}

var validate *validator.Validate

func validateUser(user *User) {
	validate = validator.New()

	err := validate.Struct(user)
}
