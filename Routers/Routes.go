package Routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bagduch/golang/Controllers"
)

func SetupRouter(r *gin.Engine) {

	grp := r.Group("/api/v1")
	{
		grp.GET("user", Controllers.GetUsers)
	}
}
