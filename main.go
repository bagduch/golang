package main

import (
	"github.com/gin-gonic/gin"
	Routes "gitlab.com/bagduch/golang/Routers"
)

func main() {
	r := gin.Default()
	Routes.SetupRouter(r)
	r.Run()
}
